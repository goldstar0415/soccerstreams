<tr>
  <td colspan="20" width="100%" class="multiple_streams">
    <div class="streamUser">
        <div class="user-image">
          <span class="acestreamname">Ace Streams</span>
        </div>
        <div class="streams-actions">
          <a href="#" onclick="event.preventDefault()" class="btn btn-sm btn-rss leaveReply" data-toggle="collapse" data-target="#comments_aceStream" >Add links ({{count($aceComments)}})</a>
        </div>
    </div>

    <div class="comments collapse" id="comments_aceStream">
      <div class="event-comments">
        
        {{-- nested comments --}}
        <div class="stream-comments-div" id="ace-comments-div">   
            @if(count($aceComments))
              @foreach($aceComments as $ac)
                @include('userAceStreamComments', ['comment'=>$ac, 'user_comment_count'=> count($aceComments), 'event_id'=> $event->event_id])
              @endforeach
            @endif   
            @if(\Illuminate\Support\Facades\Auth::check())
              {{-- registered user comment box --}}
              <form onsubmit="event.preventDefault();addAceStreamComment($(this));" class="streamCommentAdd" method="post" >
                <input type="hidden" name="event_id" value="{{ $event->event_id }}">
                <div class="form-group">
                  <textarea name="comment" class="form-control" placeholder="Your ace stream link" rows="1"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Add Stream</button>
              </form>           
            @else
              <p>Please <a href="{{ secure_url('register') }}">register</a> to add your comment or <a href="{{ secure_url('redditLogin') }}">login with Reddit.</a></p>
            @endif      
        </div>
        {{-- end of nested comments --}}
      </div>
    </div>
  </td>
</tr>