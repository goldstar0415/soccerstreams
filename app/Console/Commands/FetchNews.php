<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Corcel\Post as Corcel;
use App\NewsPost;

class FetchNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch soccer news from deferent rss feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $feed = \Feeds::make([
            'https://www.theguardian.com/football/rss',
            'http://www.shoot.co.uk/feed/',
            'https://www.theguardian.com/uk/sport/rss',
            'http://www.dailymail.co.uk/sport/index.rss',
            'http://www.independent.co.uk/sport/rss',
            'http://www.goal.com/en-gb/feeds/news?fmt=rss&ICID=HP',
            'http://www.espnfc.com/rss',
            'https://sports.yahoo.com/top/rss.xml',
            'http://feeds.skynews.com/feeds/rss/sports.xml',
            'http://www.bbc.com/sport/rss.xml',
        ]);
        $data = array(
          'title'     => $feed->get_title(),
          'permalink' => $feed->get_permalink(),
          'items'     => $feed->get_items(),
        );
        foreach ($data["items"] as $item) {
            $post = NewsPost::where('post_date', $item->get_date('Y-m-d h:m:s'))->get();

            if($post->count()) {
                continue;
            } else {
                $post = new NewsPost;
                $post->post_author = 1;
                $post->post_date = $item->get_date('Y-m-d h:m:s');
                $post->post_date_gmt = $item->get_date('Y-m-d h:m:s');
                $post->post_content = $item->get_description();
                $post->post_title = $item->get_title();
                $post->post_status = "publish";
                $post->comment_status = "open";
                $post->ping_status = "open";
                $post->post_modified = $item->get_date('Y-m-d h:m:s');
                $post->post_modified_gmt = $item->get_date('Y-m-d h:m:s');
                $post->post_parent = 0;
                $post->post_type = "post";
                $post->save();

                $post->meta->url = $item->get_permalink();
                $post->save();
            }
        }

    }
}
