<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Corcel\Post as Corcel;
use App\NewsPost;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * For soccer news testing perpose
     *
     * @return void
     * @author Risul Islam - risul321@gmail.com
     **/
    public function testNews()
    {
        // $feed = \Feeds::make([
        //     'https://www.theguardian.com/football/rss',
        //     'http://www.shoot.co.uk/feed/',
        //     'https://www.theguardian.com/uk/sport/rss',
        //     'http://www.dailymail.co.uk/sport/index.rss',
        //     'http://www.independent.co.uk/sport/rss',
        //     'http://www.goal.com/en-gb/feeds/news?fmt=rss&ICID=HP',
        //     'http://www.espnfc.com/rss',
        // ]);
        // $data = array(
        //   'title'     => $feed->get_title(),
        //   'permalink' => $feed->get_permalink(),
        //   'items'     => $feed->get_items(),
        // );
        // foreach ($data["items"] as $item) {
        //     $post = NewsPost::where('post_date', $item->get_date('Y-m-d h:m:s'))->get();

        //     if($post->count()) {
        //         continue;
        //     } else {
        //         $post = new NewsPost;
        //         $post->post_author = 1;
        //         $post->post_date = $item->get_date('Y-m-d h:m:s');
        //         $post->post_date_gmt = $item->get_date('Y-m-d h:m:s');
        //         $post->post_content = $item->get_description();
        //         $post->post_title = $item->get_title();
        //         $post->post_status = "publish";
        //         $post->comment_status = "open";
        //         $post->ping_status = "open";
        //         $post->post_modified = $item->get_date('Y-m-d h:m:s');
        //         $post->post_modified_gmt = $item->get_date('Y-m-d h:m:s');
        //         $post->post_parent = 0;
        //         $post->post_type = "post";
        //         $post->save();

        //         $post->meta->url = $item->get_permalink();
        //         $post->save();
        //     }
        // }
    }
}
