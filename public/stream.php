<script type="text/javascript" src="//cdn.jsdelivr.net/discord-widget/latest/discord-widget.min.js"></script>
<script type="text/javascript">
    discordWidget.init({
        serverId: '341369416656617493',
        title: 'Soccerstreams',
        join: false,
        alphabetical: false,
        theme: 'dark',
        hideChannels: ['Channel Name 1', 'Channel Name 2'],
        showAllUsers: true,
        allUsersDefaultState: true
    });
    discordWidget.render();
</script>
<div class="discord-widget"></div>